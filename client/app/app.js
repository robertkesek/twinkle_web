import './polyfills';

import React from 'react';
import ReactDOM from 'react-dom';
import { Layout } from './components/Layout';

ReactDOM.render(React.createElement(Layout), document.querySelector("main"));
