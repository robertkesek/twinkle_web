var gulp = require('gulp');
var source = require('vinyl-source-stream');
var browserify = require('browserify');
var babelify = require('babelify');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var minifycss = require('gulp-minify-css');
var watchify = require('watchify');
var sourcemaps = require('gulp-sourcemaps');
var uglify = require('gulp-uglify');
var buffer = require('vinyl-buffer');

var paths = {
    appJs: ['client/app/**/*.js', '!client/app/vendor/**/*', '!client/app/components/**/*'],
    css: ['client/**/*.scss'],
    staticFiles: ['client/index.html', 'client/assets/fonts/**/*', 'client/assets/images/**/*']
};

//lint js code
function lintJs() {
    return gulp.src(paths.appJs)
        .pipe(jshint({
            browser: true,
            freeze: true,
            maxdepth: 2,
            maxparams: 2,
            unused: true,
            varstmt: true,
            esnext: true
        }))
        .pipe(jshint.reporter('jshint-stylish'));
}

function watchDevelopmentJs() {
    var bundler = browserify({
        entries: './client/app/app.js',
        debug: true,
        cache: {}, // required for watchify
        packageCache: {}, // required for watchify
        fullPaths: true // required to be true only for watchify
    });

    var rebundle = function() {
        var start = Date.now();
        console.log('bundle start');

        bundler.transform(babelify.configure({
            compact: false,
            ignore: /(vendor)|(node_modules)/
        }))
            .bundle()
            .pipe(source('application.min.js'))
            .pipe(gulp.dest('client/build/javascript'))
            .on("error", function (err) { console.log("Error : " + err.message); })
            .on('end', function(){
                console.log('bundle finished in ' + (Date.now() - start) + 'ms');
            });
    };

    rebundle();
    bundler = watchify(bundler);
    bundler.on('update', rebundle);
}

/**
 * concat all app files and create mapping
 */
function compileProductionJs() {
    var bundler = browserify({
        entries: './client/app/app.js',
        debug: false
    });

    var start = Date.now();

    bundler.transform(babelify.configure({
        compact: false,
        ignore: /(vendor)|(node_modules)/
    }))
        .bundle()
        .pipe(source('application.min.js'))
        .pipe(buffer())
        .pipe(uglify())
        .pipe(gulp.dest('client/build/javascript'))
        .on("error", function (err) {
            console.log("Error : " + err.message);
        })
        .on('end', function () {
            console.log('bundle finished in ' + (Date.now() - start) + 'ms');
        });
}

function compileDevelopmentCss() {
    return gulp.src(['client/assets/scss/style.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass({'errLogToConsole': true}))
        .pipe(minifycss({
            keepSpecialComments: 0
        }))
        .pipe(concat('style.min.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('client/build/stylesheet/'));
}
/**
 * compile sass to css and minify it
 */
function compileProductionCss() {
    return gulp.src(['client/assets/scss/style.scss'])
        .pipe(sass({'errLogToConsole': true}))
        .pipe(minifycss({
            keepSpecialComments: 0
        }))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('client/build/stylesheet/'));
}

gulp.task('lintJs', lintJs);
gulp.task('compileAppJs', compileProductionJs);
gulp.task('compileProductionCss', compileProductionCss);
gulp.task('compileDevelopmentCss', compileDevelopmentCss);

gulp.task('copyStatic', function(){
    gulp.src(['client/index.html']).pipe(gulp.dest('client/build'));
    gulp.src(['client/assets/fonts/**/*']).pipe(gulp.dest('client/build/fonts/'));
    gulp.src(['client/assets/images/**/*']).pipe(gulp.dest('client/build/images/'));
});

// Rerun the task when a file changes
gulp.task('watch', function () {
    compileDevelopmentCss();
    watchDevelopmentJs();


    gulp.watch(paths.appJs, ['lintJs']);
    gulp.watch(paths.css, ['compileDevelopmentCss']);
    gulp.watch(paths.staticFiles, ['copyStatic']);
});

gulp.task('default', ['lintJs', 'compileAppJs', 'compileProductionCss', 'copyStatic']);
